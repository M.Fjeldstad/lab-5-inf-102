package INF102.lab5.graph;

import java.util.HashSet;
import java.util.Set;

public class GraphSearch<V> implements IGraphSearch<V> {

    private IGraph<V> graph;

    public GraphSearch(IGraph<V> graph) {
        this.graph = graph;
    }

    @Override
    public boolean connected(V u, V v) {
        Set<V> toDoSet = new HashSet<>();
        Set<V> doneSet = new HashSet<>();

        toDoSet.add(u);

        while (!toDoSet.isEmpty()) {

            V currentNode = toDoSet.iterator().next();
            toDoSet.remove(currentNode);

            doneSet.add(currentNode);

            if (currentNode.equals(v)) {
                return true;
            }

            for (V neighbor : graph.getNeighbourhood(currentNode)) {

                if (!doneSet.contains(neighbor)) {
                    toDoSet.add(neighbor);
                }
            }
        }

        return false;

    }
}
